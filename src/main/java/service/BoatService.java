package service;

import com.database.Boat;
import dao.BoatDao;

import java.util.List;

public class BoatService implements DaoService<Boat, String> {

    private BoatDao boatDao;

    public BoatService() {
        this.boatDao = new BoatDao();
        this.boatDao.startEntityManager();
    }

    @Override
    public void startEntityManager () {
        boatDao.startEntityManager();
    }

    @Override
    public List<Boat> getAll() {
        boatDao.beginTransaction();
        List<Boat> boats = boatDao.findAll();
        boatDao.commitTransaction();

        return boats;
    }

    @Override
    public Boat get(String id) {
        boatDao.beginTransaction();
        Boat boat = boatDao.findById(id);
        boatDao.commitTransaction();

        return boat;
    }

    @Override
    public Boat create(Boat boat) {
        boatDao.beginTransaction();
        boatDao.persist(boat);
        boatDao.commitTransaction();
        return boat;
    }

    @Override
    public void delete(Boat boat) {
        boatDao.beginTransaction();
        boatDao.delete(boat);
        boatDao.commitTransaction();
    }

    @Override
    public Boat update(Boat boat) {
        boatDao.beginTransaction();
        boatDao.update(boat);
        boatDao.commitTransaction();
        return boat;
    }

    @Override
    public void deleteAll () {
        boatDao.beginTransaction();
        boatDao.deleteAll();
        boatDao.commitTransaction();
    }

    @Override
    public void close() {
        boatDao.close();
    }

}
