package service;

import com.database.Model;
import com.database.ModelId;
import dao.ModelDao;

import java.util.List;

public class ModelService implements DaoService<Model, ModelId> {

    private ModelDao modelDao;

    public ModelService() {
        this.modelDao = new ModelDao();
        modelDao.startEntityManager();
    }

    @Override
    public void startEntityManager() {
        modelDao.startEntityManager();
    }

    @Override
    public List<Model> getAll() {
        modelDao.beginTransaction();
        List<Model> models = modelDao.findAll();
        modelDao.commitTransaction();

        return models;
    }

    @Override
    public Model get(ModelId modelId) {
        modelDao.beginTransaction();
        Model model = modelDao.findById(modelId);
        modelDao.commitTransaction();

        return model;
    }

    @Override
    public Model create(Model model) {
        modelDao.beginTransaction();
        modelDao.persist(model);
        modelDao.commitTransaction();
        return model;
    }

    @Override
    public void delete(Model model) {
        modelDao.beginTransaction();
        modelDao.delete(model);
        modelDao.commitTransaction();

    }

    @Override
    public Model update(Model model) {
        modelDao.beginTransaction();
        modelDao.update(model);
        modelDao.commitTransaction();
        return model;
    }

    @Override
    public void deleteAll() {
        modelDao.beginTransaction();
        modelDao.deleteAll();
        modelDao.commitTransaction();
    }

    @Override
    public void close() {
        modelDao.close();
    }
}
