package service;

import com.database.Rent;
import dao.RentDao;

import java.util.List;

public class RentService implements DaoService<Rent, Long> {

    private RentDao rentDao;

    public RentService() {
        this.rentDao = new RentDao();
        rentDao.startEntityManager();
    }

    @Override
    public void startEntityManager() {
        rentDao.startEntityManager();
    }

    @Override
    public List<Rent> getAll() {
        rentDao.beginTransaction();
        List<Rent> rents = rentDao.findAll();
        rentDao.commitTransaction();

        return rents;
    }

    @Override
    public Rent get(Long rentId) {
        rentDao.beginTransaction();
        Rent rent = rentDao.findById(rentId);
        rentDao.commitTransaction();

        return rent;
    }

    @Override
    public Rent create(Rent rent) {
        rentDao.beginTransaction();
        rentDao.persist(rent);
        rentDao.commitTransaction();
        return rent;
    }

    @Override
    public void delete(Rent rent) {
        rentDao.beginTransaction();
        rentDao.delete(rent);
        rentDao.commitTransaction();
    }

    @Override
    public Rent update(Rent rent) {
        rentDao.beginTransaction();
        rentDao.update(rent);
        rentDao.commitTransaction();
        return rent;
    }

    @Override
    public void deleteAll() {
        rentDao.beginTransaction();
        rentDao.deleteAll();
        rentDao.commitTransaction();
    }

    @Override
    public void close() {
        rentDao.close();
    }
}
