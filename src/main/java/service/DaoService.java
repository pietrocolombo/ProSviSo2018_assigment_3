package service;

import java.io.Serializable;
import java.util.List;

public interface DaoService<T, Id extends Serializable> {

    void startEntityManager();
    List<T> getAll();
    T get(Id id);
    T create(T t);
    void delete(T t);
    T update(T t);
    void deleteAll();
    void close();
}
