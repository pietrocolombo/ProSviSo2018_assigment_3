package service;

import com.database.Customer;
import dao.CustomerDao;
import java.util.List;

public class CustomerService implements DaoService<Customer, String>{

    private CustomerDao customerDao;

    public CustomerService() {
        this.customerDao = new CustomerDao();
        customerDao.startEntityManager();
    }

    @Override
    public void startEntityManager () {
        customerDao.startEntityManager();
    }

    @Override
    public List<Customer> getAll() {
        customerDao.beginTransaction();
        List<Customer> customers = customerDao.findAll();
        customerDao.commitTransaction();

        return customers;
    }

    @Override
    public Customer get(String id) {
        customerDao.beginTransaction();
        Customer customer = customerDao.findById(id);
        customerDao.commitTransaction();

        return customer;
    }

    @Override
    public Customer create(Customer customer) {
        customerDao.beginTransaction();
        customerDao.persist(customer);
        customerDao.commitTransaction();

        return customer;
    }

    @Override
    public void delete(Customer customer) {
        customerDao.beginTransaction();
        customerDao.delete(customer);
        customerDao.commitTransaction();
    }

    @Override
    public Customer update(Customer customer) {
        customerDao.beginTransaction();
        customerDao.update(customer);
        customerDao.commitTransaction();

        return customer;
    }

    @Override
    public void deleteAll () {
        customerDao.beginTransaction();
        customerDao.deleteAll();
        customerDao.commitTransaction();
    }

    @Override
    public void close() {
        customerDao.close();
    }

}
