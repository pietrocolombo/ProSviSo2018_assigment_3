package service;

import com.database.BoatMooring;
import com.database.BoatMooringId;
import dao.BoatMooringDao;

import java.util.List;

public class    BoatMooringService implements DaoService<BoatMooring, BoatMooringId> {

    private BoatMooringDao boatMooringDao;

    public BoatMooringService() {
        this.boatMooringDao = new BoatMooringDao();
        boatMooringDao.startEntityManager();
    }

    @Override
    public void startEntityManager() {
        boatMooringDao.startEntityManager();
    }

    @Override
    public List<BoatMooring> getAll() {
        boatMooringDao.beginTransaction();
        List<BoatMooring> boatMoorings = boatMooringDao.findAll();
        boatMooringDao.commitTransaction();

        return boatMoorings;
    }

    @Override
    public BoatMooring get(BoatMooringId boatMooringId) {
        boatMooringDao.beginTransaction();
        BoatMooring boatMooring = boatMooringDao.findById(boatMooringId);
        boatMooringDao.commitTransaction();

        return boatMooring;
    }

    @Override
    public BoatMooring create(BoatMooring boatMooring) {
        boatMooringDao.beginTransaction();
        boatMooringDao.persist(boatMooring);
        boatMooringDao.commitTransaction();
        return boatMooring;
    }

    @Override
    public void delete(BoatMooring boatMooring) {
        boatMooringDao.beginTransaction();
        boatMooringDao.delete(boatMooring);
        boatMooringDao.commitTransaction();
    }

    @Override
    public BoatMooring update(BoatMooring boatMooring) {
        boatMooringDao.beginTransaction();
        boatMooringDao.update(boatMooring);
        boatMooringDao.commitTransaction();
        return boatMooring;
    }

    @Override
    public void deleteAll() {
        boatMooringDao.beginTransaction();
        boatMooringDao.deleteAll();
        boatMooringDao.commitTransaction();
    }

    @Override
    public void close() {
        boatMooringDao.close();
    }

}
