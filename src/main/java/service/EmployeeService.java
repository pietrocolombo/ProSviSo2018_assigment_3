package service;

import com.database.Employee;
import dao.EmployeeDao;

import java.util.List;

public class EmployeeService implements DaoService<Employee, String> {

    private EmployeeDao employeeDao;

    public EmployeeService() {
        this.employeeDao = new EmployeeDao();
        employeeDao.startEntityManager();
    }

    @Override
    public void startEntityManager() {
        employeeDao.startEntityManager();
    }

    @Override
    public List<Employee> getAll() {
        employeeDao.beginTransaction();
        List<Employee> employees = employeeDao.findAll();
        employeeDao.commitTransaction();

        return employees;
    }

    @Override
    public Employee get(String id) {
        employeeDao.beginTransaction();
        Employee employee = employeeDao.findById(id);
        employeeDao.commitTransaction();

        return  employee;
    }

    @Override
    public Employee create(Employee employee) {
        employeeDao.beginTransaction();
        employeeDao.persist(employee);
        employeeDao.commitTransaction();

        return employee;
    }

    @Override
    public void delete(Employee employee) {
        employeeDao.beginTransaction();
        employeeDao.delete(employee);
        employeeDao.commitTransaction();
    }

    @Override
    public Employee update(Employee employee) {
        employeeDao.beginTransaction();
        employeeDao.update(employee);
        employeeDao.commitTransaction();

        return employee;
    }

    @Override
    public void deleteAll() {
        employeeDao.beginTransaction();
        employeeDao.deleteAll();
        employeeDao.commitTransaction();
    }

    @Override
    public void close() {
        employeeDao.close();
    }
}
