package dao;

import com.database.Boat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Table;
import java.util.List;

public class BoatDao implements DaoInterface<Boat, String> {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public BoatDao() {}

    @Override
    public void startEntityManager () {
        entityManagerFactory = Persistence.createEntityManagerFactory("port");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void beginTransaction () {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commitTransaction () {
        // force the data to be persisted
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(Boat entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(Boat entity) {
        entityManager.merge(entity);
    }

    @Override
    public Boat findById(String id) {
        return entityManager.find(Boat.class, id);
    }

    @Override
    public void delete(Boat entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<Boat> findAll() {
        return entityManager.createNativeQuery("SELECT * FROM " + Boat.class.getAnnotation(Table.class).name()).getResultList();
    }

    @Override
    public void deleteAll() {
        entityManager.createNativeQuery("DELETE FROM " + Boat.class.getAnnotation(Table.class).name() + " WHERE 1").executeUpdate();
    }

    @Override
    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }

}

