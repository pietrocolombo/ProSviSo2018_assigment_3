package dao;

import com.database.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Table;
import java.util.List;

public class EmployeeDao implements DaoInterface<Employee, String> {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public EmployeeDao() {}

    @Override
    public void startEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("port");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commitTransaction() {
        // force the data to be persisted
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(Employee entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(Employee entity) {
        entityManager.merge(entity);
    }

    @Override
    public Employee findById(String id) {
        return entityManager.find(Employee.class, id);
    }

    @Override
    public void delete(Employee entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<Employee> findAll() {
        return entityManager.createNativeQuery("SELECT * FROM " + Employee.class.getAnnotation(Table.class).name()).getResultList();
    }

    @Override
    public void deleteAll() {
        entityManager.createNativeQuery("DELETE FROM " + Employee.class.getAnnotation(Table.class).name() + " WHERE 1").executeUpdate();
    }

    @Override
    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }
}

