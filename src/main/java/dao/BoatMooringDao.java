package dao;

import com.database.BoatMooring;
import com.database.BoatMooringId;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Table;
import java.util.List;

public class BoatMooringDao implements DaoInterface<BoatMooring, BoatMooringId> {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public BoatMooringDao() {}

    @Override
    public void startEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("port");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commitTransaction() {
        // force the data to be persisted
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(BoatMooring entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(BoatMooring entity) {
        entityManager.merge(entity);
    }

    @Override
    public BoatMooring findById(BoatMooringId id) {
        return entityManager.find(BoatMooring.class, id);
    }

    @Override
    public void delete(BoatMooring entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<BoatMooring> findAll() {
        return entityManager.createNativeQuery("SELECT * FROM " + BoatMooring.class.getAnnotation(Table.class).name()).getResultList();
    }

    @Override
    public void deleteAll() {
        entityManager.createNativeQuery("DELETE FROM " + BoatMooring.class.getAnnotation(Table.class).name() + " WHERE 1").executeUpdate();
    }

    @Override
    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }
}
