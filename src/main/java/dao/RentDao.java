package dao;

import com.database.Rent;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Table;
import java.util.List;

public class RentDao implements DaoInterface<Rent, Long> {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public RentDao() {}

    @Override
    public void startEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("port");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commitTransaction() {
        // force the data to be persisted
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(Rent entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(Rent entity) {
        entityManager.merge(entity);
    }

    @Override
    public Rent findById(Long rentId) {
        return entityManager.find(Rent.class, rentId);
    }

    @Override
    public void delete(Rent entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<Rent> findAll() {
        return entityManager.createNativeQuery("SELECT * FROM " + Rent.class.getAnnotation(Table.class).name()).getResultList();
    }

    @Override
    public void deleteAll() {
        entityManager.createNativeQuery("DELETE FROM " + Rent.class.getAnnotation(Table.class).name() + " WHERE 1").executeUpdate();
    }

    @Override
    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }
}
