package dao;

import com.database.Model;
import com.database.ModelId;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Table;
import java.util.List;

public class ModelDao implements DaoInterface<Model, ModelId> {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public ModelDao() {}

    @Override
    public void startEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("port");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commitTransaction() {
        // force the data to be persisted
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(Model entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(Model entity) {
        entityManager.merge(entity);
    }

    @Override
    public Model findById(ModelId id) {
        return entityManager.find(Model.class, id);
    }

    @Override
    public void delete(Model entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<Model> findAll() {
        return entityManager.createNativeQuery("SELECT * FROM " + Model.class.getAnnotation(Table.class).name()).getResultList();
    }

    @Override
    public void deleteAll() {
        entityManager.createNativeQuery("DELETE FROM " + Model.class.getAnnotation(Table.class).name() + " WHERE 1").executeUpdate();
    }

    @Override
    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }
}
