package dao;

import com.database.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Table;
import java.util.List;

public class CustomerDao implements DaoInterface<Customer, String> {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public CustomerDao() {}

    @Override
    public void startEntityManager () {
        entityManagerFactory = Persistence.createEntityManagerFactory("port");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void beginTransaction () {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commitTransaction () {
        // force the data to be persisted
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(Customer entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(Customer entity) {
        entityManager.merge(entity);
    }

    @Override
    public Customer findById(String id) {
        return entityManager.find(Customer.class, id);
    }

    @Override
    public void delete(Customer entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<Customer> findAll() {
        return entityManager.createNativeQuery("SELECT * FROM " + Customer.class.getAnnotation(Table.class).name()).getResultList();
    }

    @Override
    public void deleteAll() {
        entityManager.createNativeQuery("DELETE FROM " + Customer.class.getAnnotation(Table.class).name() + " WHERE 1").executeUpdate();
    }

    @Override
    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }
}