package com.database;

import javax.persistence.*;

@Entity
@Table (name = "rent")
public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column (name = "begin")
    private String begin;

    @Column (name = "end")
    private String end;

    public Rent(String begin, String end, Customer customer, Boat boat) {
        this.begin = begin;
        this.end = end;
        this.customer = customer;
        this.boat = boat;
    }

    public Rent() {
    }

    public Long getId() {
        return id;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Rent{" +
                "Id='" + id + '\'' +
                ", begin='" + begin + '\'' +
                ", end='" + end + '\'' +
                ", customer = " + customer.toString() + '\'' +
                ", boat = " + boat.toString() + '\'' +
                '}';
    }


    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @PrimaryKeyJoinColumn(name = "customer_id")
    private Customer customer;


    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @PrimaryKeyJoinColumn(name = "boat_id")
    private Boat boat;

}

