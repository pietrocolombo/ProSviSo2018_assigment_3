package com.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.IdClass;


@Entity
@IdClass(BoatMooringId.class)
@Table(name = "boat_mooring")
public class BoatMooring {

    @Id
    @Column(name = "id_pier")
    private Integer idPier;

    @Id
    @Column(name = "number")
    private Integer number;

    @Column(name = "length")
    private Double length;

    @Column(name = "width")
    private Double width;

    @Column(name = "depth")
    private Double depth;

    public BoatMooring(Integer idPier, Integer number, Double length, Double width, Double depth) {
        this.idPier = idPier;
        this.number = number;
        this.length = length;
        this.width = width;
        this.depth = depth;
    }

    public BoatMooring() {
    }

    public Integer getIdPier() {
        return idPier;
    }

    public void setIdPier(Integer idPier) {
        this.idPier = idPier;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getDepth() {
        return depth;
    }

    public void setDepth(Double depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return "BoatMooring{" +
                "idPier=" + idPier +
                ", number=" + number +
                ", length=" + length +
                ", width=" + width +
                ", depth=" + depth +
                '}';
    }
}
