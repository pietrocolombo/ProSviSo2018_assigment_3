package com.database;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "customer")
public class Customer extends Person {

    @Column (name = "shipowner")
    private boolean shipowner;

    public Customer(String fiscalCode, String name, String surname, String country, String city, String address, String birthday, boolean shipowner) {
        super(fiscalCode, name, surname, country, city, address, birthday);
        this.shipowner = shipowner;
    }

    public Customer() {
    }

    public boolean isShipowner() {
        return shipowner;
    }

    public void setShipowner(boolean shipowner) {
        this.shipowner = shipowner;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "shipowner=" + shipowner +
                '}';
    }


    @ManyToOne
    @JoinColumn (name = "shipowner_id")
    private Customer shipowner_id;

    @OneToMany(mappedBy="customer")
    private List<Rent> boats;

}
