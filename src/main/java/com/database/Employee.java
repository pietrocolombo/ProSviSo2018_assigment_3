package com.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee extends Person {

    @Column (name = "task")
    private String task;

    @Column (name = "salary")
    private double salary;

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "task='" + task + '\'' +
                ", salary=" + salary +
                '}';
    }

    public Employee(String fiscalCode, String name, String surname, String country, String city, String address, String birthday, String task, double salary) {
        super(fiscalCode, name, surname, country, city, address, birthday);
        this.task = task;
        this.salary = salary;
    }

    public Employee() {
    }
}
