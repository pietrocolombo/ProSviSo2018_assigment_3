package com.database;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "boat")
public class Boat {

    @Id
    @Column(name = "registration_number")
    private String registrationNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "flag")
    private String flag;

    @Column(name = "year_of_construction")
    private Integer yearOfConstruction;

    public Boat(String registrationNumber, String name, String flag, Integer yearOfConstruction, Model model, BoatMooring boatMooring, Customer customer, List<Rent> customers) {
        this.registrationNumber = registrationNumber;
        this.name = name;
        this.flag = flag;
        this.yearOfConstruction = yearOfConstruction;
        this.model = model;
        this.boatMooring = boatMooring;
        this.customer = customer;
        this.customers = customers;
    }

    public Boat() {
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getYearOfConstruction() {
        return yearOfConstruction;
    }

    public void setYearOfConstruction(Integer yearOfConstruction) {
        this.yearOfConstruction = yearOfConstruction;
    }

    @Override
    public String toString() {
        return "Boat{" +
                "registrationNumber='" + registrationNumber + '\'' +
                ", flag='" + flag + '\'' +
                ", yearOfConstruction='" + yearOfConstruction + '\'' +
                '}';
    }

    @ManyToOne
    @JoinColumns ({@JoinColumn(name = "fk_model"), @JoinColumn(name= "fk_shipyard")})
    private Model model;

    @OneToOne
    @JoinColumns ({@JoinColumn(name = "fk_id_pier"), @JoinColumn(name= "fk_number")})
    private BoatMooring boatMooring;

    @ManyToOne
    @JoinColumn (name = "fk_customer_id")
    private Customer customer;

    @OneToMany(mappedBy="boat")
    private List<Rent> customers;

}
