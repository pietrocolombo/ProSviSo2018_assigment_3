package com.database;

import java.io.Serializable;

public class ModelId implements Serializable {

    private String model;
    private String shipyard;

    public ModelId() {}

    public ModelId(String model, String shipyard) {
        this.model = model;
        this.shipyard = shipyard;
    }
}
