package com.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.IdClass;


@Entity
@IdClass(ModelId.class)
@Table(name = "model")
public class Model {

    @Id
    @Column(name = "model", nullable = false)
    private String model;

    @Id
    @Column(name = "shipyard", nullable = false)
    private String shipyard;

    @Column(name = "type")
    private String type;

    @Column(name = "length")
    private Double length;

    @Column(name = "width")
    private Double width;

    @Column(name = "draft")
    private Double draft;

    @Column(name = "displacement")
    private Integer displacement;

    public Model(String model, String shipyard, String type, Double length, Double width, Double draft, Integer displacement) {
        this.model = model;
        this.shipyard = shipyard;
        this.type = type;
        this.length = length;
        this.width = width;
        this.draft = draft;
        this.displacement = displacement;
    }

    public Model() {
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getShipyard() {
        return shipyard;
    }

    public void setShipyard(String shipyard) {
        this.shipyard = shipyard;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getDraft() {
        return draft;
    }

    public void setDraft(Double draft) {
        this.draft = draft;
    }

    public Integer getDisplacement() {
        return displacement;
    }

    public void setDisplacement(Integer displacement) {
        this.displacement = displacement;
    }

    @Override
    public String toString() {
        return "Model{" +
                "model='" + model + '\'' +
                ", shipyard='" + shipyard + '\'' +
                ", type='" + type + '\'' +
                ", length=" + length +
                ", width=" + width +
                ", draft=" + draft +
                ", displacement=" + displacement +
                '}';
    }
}
