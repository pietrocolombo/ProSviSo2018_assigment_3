package com.database;

import java.io.Serializable;

public class BoatMooringId implements Serializable {

    private Integer idPier;
    private Integer number;

    public BoatMooringId() {}

    public BoatMooringId(Integer idPier, Integer number){
        this.idPier = idPier;
        this.number = number;
    }

}
