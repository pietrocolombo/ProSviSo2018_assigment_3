package entityTest;

import com.database.Boat;
import com.database.BoatMooring;
import com.database.Customer;
import com.database.Model;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.BoatMooringService;
import service.BoatService;
import service.CustomerService;
import service.ModelService;

import java.util.List;

import static org.junit.Assert.*;

public class BoatTest {

    static private BoatService boatService;

    @BeforeClass
    public static void beforeClass() {
        boatService = new BoatService();
    }

    @Before
    public void before() {
        boatService.startEntityManager();
        boatService.deleteAll();

        // add Customer
        CustomerService customerService = new CustomerService();
        customerService.startEntityManager();
        customerService.deleteAll();
        Customer cAdd = new Customer("RDCCRL95H26D286Y", "Carlo", "Radice", "italy", "Desio", "via sciesa", "12/12/1946", true);
        customerService.create(cAdd);
        customerService.close();

        // add BoatMooting
        BoatMooringService boatMooringService = new BoatMooringService();
        boatMooringService.startEntityManager();
        boatMooringService.deleteAll();
        BoatMooring bmAdd = new BoatMooring(1, 1, 80.50, 13.0, 4.0);
        boatMooringService.create(bmAdd);
        boatMooringService.close();

        // add Model
        ModelService modelService = new ModelService();
        modelService.startEntityManager();
        modelService.deleteAll();
        Model mAdd = new Model("White Pearl", "Nobiskrug", "Mega Yacht", 142.8, 24.80, 8.00, 10000);
        modelService.create(mAdd);
        modelService.close();

        Boat bAdd = new Boat("AZ 3717 ZW", "Sailing yacht A", "Cayman Islands", 2010, mAdd, bmAdd, cAdd, null);
        boatService.create(bAdd);
    }

    @After
    public void after() {
        boatService.deleteAll();
        boatService.close();
    }

    @Test
    public void create() {
        boatService.close();
        boatService.startEntityManager();
        boatService.deleteAll();

        // add Customer
        CustomerService customerService = new CustomerService();
        customerService.startEntityManager();
        customerService.deleteAll();
        Customer cAdd = new Customer("RDCCRL95H26D286Y", "Carlo", "Radice", "italy", "Desio", "via sciesa", "12/12/1946", true);
        customerService.create(cAdd);
        customerService.close();

        // add BoatMooting
        BoatMooringService boatMooringService = new BoatMooringService();
        boatMooringService.startEntityManager();
        boatMooringService.deleteAll();
        BoatMooring bmAdd = new BoatMooring(1, 1, 80.50, 13.0, 4.0);
        boatMooringService.create(bmAdd);
        boatMooringService.close();

        // add Model
        ModelService modelService = new ModelService();
        modelService.startEntityManager();
        modelService.deleteAll();
        Model mAdd = new Model("White Pearl", "Nobiskrug", "Mega Yacht", 142.8, 24.80, 8.00, 10000);
        modelService.create(mAdd);
        modelService.close();

        Boat bAdd = new Boat("AZ 3717 ZW", "Sailing yacht A", "Cayman Islands", 2010, mAdd, bmAdd, cAdd, null);
        boatService.create(bAdd);

        Boat bGet = boatService.get("AZ 3717 ZW");
        assertEquals(bAdd.getRegistrationNumber(), bGet.getRegistrationNumber());
        assertEquals(bAdd.getName(), bGet.getName());
        assertEquals(bAdd.getFlag(), bGet.getFlag());
    }

    @Test
    public void search() {
        String registrationNumber = "AZ 3717 ZW";
        Boat sBoat = boatService.get(registrationNumber);
        assertEquals(sBoat.getRegistrationNumber(), registrationNumber);
        assertEquals(sBoat.getName(), "Sailing yacht A");
    }

    @Test
    public void update() {
        Customer cAdd = new Customer("RDCCRL95H26D286Y", "Carlo", "Radice", "italy", "Desio", "via sciesa", "12/12/1946", true);
        BoatMooring bmAdd = new BoatMooring(1, 1, 80.50, 13.0, 4.0);
        Model mAdd = new Model("White Pearl", "Nobiskrug", "Mega Yacht", 142.8, 24.80, 8.00, 10000);
        Boat bAdd = new Boat("AZ 3717 ZW", "Sailing yacht A", "Cayman Islands", 2015, mAdd, bmAdd, cAdd, null);
        boatService.update(bAdd);

        Boat bGet = boatService.get(bAdd.getRegistrationNumber());

        assertNotEquals(java.util.Optional.of(2010), bGet.getYearOfConstruction());
        assertEquals(bGet.getYearOfConstruction(), bAdd.getYearOfConstruction());
    }

    @Test
    public void getAll() {
        List<Boat> boatList = boatService.getAll();
        assertEquals(1, boatList.size());
    }

    @Test
    public void delete() {
        String registrationNumber = "AZ 3717 ZW";
        Boat bGet = boatService.get(registrationNumber);
        boatService.delete(bGet);
        bGet = boatService.get(registrationNumber);
        assertNull(bGet);
    }

    @Test
    public void deleteAll() {
        boatService.deleteAll();
        List<Boat> boatList = boatService.getAll();
        assertEquals(0, boatList.size());
    }

}
