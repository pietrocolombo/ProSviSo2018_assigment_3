package entityTest;

import com.database.Model;
import com.database.ModelId;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ModelService;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotEquals;

public class ModelTest {

    static private ModelService modelService;

    @BeforeClass
    public static void beforeClass() {
        modelService = new ModelService();
    }

    @Before
    public void before() {
        modelService.startEntityManager();
        modelService.deleteAll();
        Model mAdd = new Model("White Pearl", "Nobiskrug", "Mega Yacht", 142.8, 24.80, 8.00, 10000);
        modelService.create(mAdd);
        Model nmAdd = new Model("Eclipse", "Blohm & Voss", "Mega Yacht", 162.5, 22.00, 5.90, 13000);
        modelService.create(nmAdd);
    }

    @After
    public void after() {
        modelService.deleteAll();
        modelService.close();
    }

    @Test
    public void create() {
        Model mAdd = new Model("Azzam", "Lurssen Yachts", "Mega Yacht", 180.6, 20.80, 4.30, 10000);
        modelService.create(mAdd);

        ModelId id = new ModelId("Azzam", "Lurssen Yachts");
        Model mGet = modelService.get(id);
        assertEquals(mAdd.getModel(), mGet.getModel());
        assertEquals(mAdd.getLength(), mGet.getLength());
    }

    @Test
    public void search() {
        ModelId id = new ModelId("White Pearl", "Nobiskrug");
        Model mGet = modelService.get(id);
        assertEquals(mGet.getDraft(), 8.0 );
        assertEquals(mGet.getType(), "Mega Yacht");
    }

    @Test
    public void update() {
        Model mAdd = new Model("White Pearl", "Nobiskrug", "Mega Yacht", 142.8, 24.00, 8.00, 10000);
        modelService.update(mAdd);

        ModelId id = new ModelId("White Pearl", "Nobiskrug");
        Model mGet = modelService.get(id);

        assertNotEquals(java.util.Optional.of(142.8), mGet.getLength());
        assertEquals(mAdd.getLength(), mGet.getLength());
    }

    @Test
    public void getAll() {
        List<Model>  modelList = modelService.getAll();
        assertEquals(2,modelList.size());
    }

    @Test
    public void delete() {
        ModelId id = new ModelId("White Pearl", "Nobiskrug");
        Model mGet = modelService.get(id);
        modelService.delete(mGet);
        mGet = modelService.get(id);
        assertNull(mGet);
    }

    @Test
    public void deleteAll() {
        modelService.deleteAll();
        List<Model>  modelList = modelService.getAll();
        assertEquals(0,modelList.size());
    }
}
