package entityTest;

import com.database.BoatMooring;
import com.database.BoatMooringId;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.BoatMooringService;
import service.BoatService;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

public class BoatMooringTest {

    static private BoatMooringService boatMooringService;

    @BeforeClass
    public static void beforeClass() {
        boatMooringService = new BoatMooringService();
    }

    @Before
    public void before() {
        BoatService boatService = new BoatService();
        boatService.startEntityManager();
        boatService.deleteAll();
        boatMooringService.startEntityManager();
        boatMooringService.deleteAll();
        BoatMooring bAdd = new BoatMooring(1, 1, 100.30, 10.30,5.6);
        boatMooringService.create(bAdd);
        BoatMooring bNew = new BoatMooring(1,2,80.0,8.3,5.2);
        boatMooringService.create(bNew);
    }

    @After
    public void after() {
        boatMooringService.deleteAll();
        boatMooringService.close();
    }

    @Test
    public void create() {
        BoatMooring bAdd = new BoatMooring(2, 1, 100.30, 10.30,5.6);
        boatMooringService.create(bAdd);
        BoatMooring bGet = boatMooringService.get(new BoatMooringId(bAdd.getIdPier(), bAdd.getNumber()));
        assertEquals(bAdd.getIdPier(), bGet.getIdPier());
        assertEquals(bAdd.getNumber(), bGet.getNumber());
        assertEquals(bAdd.getLength(), bGet.getLength());
    }

    @Test
    public void search() {
        BoatMooringId id = new BoatMooringId(1, 2);
        BoatMooring bFind = boatMooringService.get(id);
        assertEquals(1, (int)bFind.getIdPier());
        assertEquals(2, (int)bFind.getNumber());
    }

    @Test
    public void update() {
        BoatMooring bUpdate = new BoatMooring(1,2,20.0,8.3,5.2);
        boatMooringService.update(bUpdate);

        BoatMooringId id = new BoatMooringId(1, 2);
        BoatMooring bGet = boatMooringService.get(id);
        assertNotEquals(100, bGet.getLength());
        assertEquals(bGet.getLength(), bUpdate.getLength());
    }

    @Test
    public void getAll() {
        List<BoatMooring> boatMooringList = boatMooringService.getAll();
        assertEquals(2,boatMooringList.size());
    }

    @Test
    public void delete() {
        BoatMooringId id = new BoatMooringId(1, 2);
        BoatMooring bGet = boatMooringService.get(id);
        boatMooringService.delete(bGet);
        bGet = boatMooringService.get(id);
        assertNull(bGet);
    }

    @Test
    public void deleteAll() {
        boatMooringService.deleteAll();
        List<BoatMooring> boatMooringList = boatMooringService.getAll();
        assertEquals(0,boatMooringList.size());
    }
}
