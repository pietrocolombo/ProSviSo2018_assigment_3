package entityTest;

import com.database.*;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.*;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

public class RentTest {

    static private RentService rentService;
    private Long rentId;

    @BeforeClass
    public static void beforeClass() {
        rentService = new RentService();
    }

    @Before
    public void before() {
        rentService.startEntityManager();
        rentService.deleteAll();

        BoatService boatService = new BoatService();
        boatService.startEntityManager();
        boatService.deleteAll();

        ModelService modelService = new ModelService();
        modelService.startEntityManager();
        modelService.deleteAll();

        BoatMooringService boatMooringService = new BoatMooringService();
        boatMooringService.startEntityManager();
        boatMooringService.deleteAll();

        CustomerService customerService = new CustomerService();
        customerService.startEntityManager();
        customerService.deleteAll();

        // add Customer
        Customer crAdd = new Customer("RDCCRL95H26D286Y", "Carlo", "Radice", "italy", "Desio", "via sciesa", "12/12/1946", true);
        customerService.create(crAdd);

        // add Boat
        // add Customer
        Customer cAdd = new Customer("CLMPTR93T18E507Q","Pietro", "Radice","italy", "Mandello","via Manzoni", "18/12/1993", true);
        customerService.create(cAdd);
        customerService.close();

        // add BoatMooting
        BoatMooring bmAdd = new BoatMooring(1, 1, 80.50, 13.0, 4.0);
        boatMooringService.create(bmAdd);
        boatMooringService.close();

        // add Model

        Model mAdd = new Model("White Pearl", "Nobiskrug", "Mega Yacht", 142.8, 24.80, 8.00, 10000);
        modelService.create(mAdd);
        modelService.close();

        // add boat
        Boat bAdd = new Boat("AZ 3717 ZW", "Sailing yacht A", "Cayman Islands", 2010, mAdd, bmAdd, cAdd, null);
        boatService.create(bAdd);
        boatService.close();

        Rent rAdd = new Rent("16/06/2018","21/06/2018", crAdd, bAdd);

        rentService.create(rAdd);
        rentId = rAdd.getId();
    }

    @After
    public void after() {
        rentService.deleteAll();
        rentService.close();
    }

    @Test
    public void create() {
        rentService.close();
        rentService.startEntityManager();
        rentService.deleteAll();

        BoatService boatService = new BoatService();
        boatService.startEntityManager();
        boatService.deleteAll();

        ModelService modelService = new ModelService();
        modelService.startEntityManager();
        modelService.deleteAll();

        BoatMooringService boatMooringService = new BoatMooringService();
        boatMooringService.startEntityManager();
        boatMooringService.deleteAll();

        CustomerService customerService = new CustomerService();
        customerService.startEntityManager();
        customerService.deleteAll();

        // add Customer
        Customer crAdd = new Customer("RDCCRL95H26D286Y", "Carlo", "Radice", "italy", "Desio", "via sciesa", "12/12/1946", true);
        customerService.create(crAdd);

        // add Boat
        // add Customer
        Customer cAdd = new Customer("CLMPTR93T18E507Q","Pietro", "Radice","italy", "Mandello","via Manzoni", "18/12/1993", true);
        customerService.create(cAdd);
        customerService.close();

        // add BoatMooting
        BoatMooring bmAdd = new BoatMooring(1, 1, 80.50, 13.0, 4.0);
        boatMooringService.create(bmAdd);
        boatMooringService.close();

        // add Model

        Model mAdd = new Model("White Pearl", "Nobiskrug", "Mega Yacht", 142.8, 24.80, 8.00, 10000);
        modelService.create(mAdd);
        modelService.close();

        // add boat
        Boat bAdd = new Boat("AZ 3717 ZW", "Sailing yacht A", "Cayman Islands", 2010, mAdd, bmAdd, cAdd, null);
        boatService.create(bAdd);
        boatService.close();

        Rent rAdd = new Rent("16/06/2018","21/06/2018", crAdd, bAdd);

        rentService.create(rAdd);
    }

    @Test
    public void search() {
        Rent sRent = rentService.get(rentId);
        assertEquals(sRent.getId(), rentId);
    }

    @Test
    public void update() {
        Rent dRent = rentService.get(rentId);
        dRent.setBegin("18/06/2018");
        rentService.update(dRent);

        Rent sRent = rentService.get(rentId);
        assertEquals("18/06/2018", sRent.getBegin());
    }

    @Test
    public void getAll() {
        List<Rent> rentList = rentService.getAll();
        assertEquals(1, rentList.size());

    }

    @Test
    public void delete() {
        Rent dRent = rentService.get(rentId);
        rentService.delete(dRent);
        dRent = rentService.get(rentId);
        assertNull(dRent);
    }

    @Test
    public void deleteAll() {
        rentService.deleteAll();
        List<Rent> rentList = rentService.getAll();
        assertEquals(0,rentList.size());
    }
}
