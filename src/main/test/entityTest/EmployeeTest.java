package entityTest;

import com.database.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.EmployeeService;

import java.util.List;

import static org.junit.Assert.*;

public class EmployeeTest {

    static private EmployeeService employeeService;

    @BeforeClass
    public static void beforeClass() {
        employeeService = new EmployeeService();
    }

    @Before
    public void before() {
        employeeService.startEntityManager();
        employeeService.deleteAll();
        Employee eAdd = new Employee("MRCRSS80A01E507L", "Marco", "Rossi", "italy", "Lecco", "via roma", "1/01/1980","mooring man",2000);
        employeeService.create(eAdd);
        Employee eNew = new Employee("MRARSS80A01E507H", "Mario", "Rossi", "italy", "Lecco", "via roma", "1/01/1980","mooring man",2000);
        employeeService.create(eNew);
    }

    @After
    public void after() {
        employeeService.deleteAll();
        employeeService.close();
    }

    @Test
    public void create() {
        Employee eAdd = new Employee("FGLMRC87B09E507W", "Marco", "Fagioli", "italy", "Mandello", "via roma", "10/02/1996", "mechanic", 3000);
        employeeService.create(eAdd);
        Employee eGet = employeeService.get(eAdd.getFiscalCode());
        assertEquals(eAdd.getFiscalCode(), eGet.getFiscalCode());
        assertEquals(eAdd.getName(), eGet.getName());
        assertEquals(eAdd.getSurname(), eGet.getSurname());
    }

    @Test
    public void search() {
        String cf = "MRCRSS80A01E507L";
        Employee eFind = employeeService.get(cf);
        assertEquals(eFind.getFiscalCode(), cf);
    }

    @Test
    public void update() {
        Employee eUpdate = new Employee("MRARSS80A01E507H", "Mario", "Rossi", "italy", "Lecco", "via manzoni", "1/01/1980","Ormeggiatore",2000);
        employeeService.update(eUpdate);

        Employee eGet = employeeService.get(eUpdate.getFiscalCode());

        assertNotEquals("via roma", eGet.getAddress());
        assertEquals(eGet.getAddress(), eUpdate.getAddress());
    }

    @Test
    public void getAll() {
        List<Employee> employeeList = employeeService.getAll();
        assertEquals(2, employeeList.size());
    }

    @Test
    public void delete() {
        String cf = "MRCRSS80A01E507L";
        Employee eGet = employeeService.get(cf);
        employeeService.delete(eGet);
        eGet = employeeService.get(cf);
        assertNull(eGet);
    }

    @Test
    public void deleteAll() {
        employeeService.deleteAll();
        List<Employee> employeeList = employeeService.getAll();
        assertEquals(0, employeeList.size());
    }
}
