package entityTest;

import com.database.Customer;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.CustomerService;

import java.util.List;

import static org.junit.Assert.*;

public class CustomerTest {

    static private CustomerService customerService;

    @BeforeClass
    public static void beforeClass() {
        customerService = new CustomerService();
    }

    @Before
    public void before() {
        customerService.startEntityManager();
        customerService.deleteAll();
        Customer cAdd = new Customer("RDCCRL95H26D286Y","Carlo", "Radice","italy", "Desio","via sciesa", "12/12/1946", true);
        customerService.create(cAdd);
        Customer cNew = new Customer("CLMPTR93T18E507Q","Pietro", "Radice","italy", "Mandello","via Manzoni", "18/12/1993", true);
        customerService.create(cNew);
    }

    @After
    public void after() {
        customerService.deleteAll();
        customerService.close();
    }

    @Test
    public void create() {
        Customer cAdd = new Customer("FGLMRC87B09E507W","Marco", "Fagioli","italy", "mandello","via sciesa", "12/12/1956", true);
        customerService.create(cAdd);
        Customer cGet = customerService.get(cAdd.getFiscalCode());
        assertEquals(cAdd.getFiscalCode(), cGet.getFiscalCode());
        assertEquals(cAdd.getName(), cGet.getName());
        assertEquals(cAdd.getSurname(), cGet.getSurname());
    }

    @Test
    public void search() {
        String cf = "RDCCRL95H26D286Y";
        Customer cFind = customerService.get(cf);
        assertEquals(cFind.getFiscalCode(), cf);
    }

    @Test
    public void update() {
        Customer cUpdate = new Customer("CLMPTR93T18E507Q","Pietro", "Colombo","italy", "Mandello","via Manzoni", "18/12/1993", true);
        customerService.update(cUpdate);

        Customer cGet = customerService.get(cUpdate.getFiscalCode());

        assertNotEquals("Colonmbo", cGet.getSurname());
        assertEquals(cGet.getSurname(), cUpdate.getSurname());
    }

    @Test
    public void getAll() {
        List<Customer> customerList = customerService.getAll();
        assertEquals(2, customerList.size());
    }

    @Test
    public void delete() {
        String cf = "RDCCRL95H26D286Y";
        Customer cGet = customerService.get(cf);
        customerService.delete(cGet);
        cGet = customerService.get(cf);
        assertNull(cGet);
    }

    @Test
    public void deleteAll() {
        customerService.deleteAll();
        List<Customer> customerList = customerService.getAll();
        assertEquals(0, customerList.size());
    }

}
