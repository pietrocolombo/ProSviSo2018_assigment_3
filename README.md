# ProSviSo2018_assigment_3

#
Carlo Radice 807159

Pietro Colombo 793679

## Installation
- MySQL versione 8.x

- istallazione IntelliJ IDEA student version

## Setup MySQL

- creation user in the database of mysql with name:port and password: assigment3
CREATE USER 'port'@'localhost' IDENTIFIED BY 'assignment3';

- Assignment privileges to the new user
GRANT ALL PRIVILEGES ON *.* TO 'port'@'localhost';
FLUSH PRIVILEGES;

## Configuration project in IntelliJ

Open IntelliJ and import the project that you have clone
![](img/import_1.png)
![](img/import_2.png)
![](img/import_3.png)
![](img/import_4.png)
![](img/import_5.png)
![](img/import_6.png)

Enable Auto-Import for Maven
![](img/import_7.png)

Now you can run all Test if you have add a "port" username in MySQL 

![](img/import_8.png)